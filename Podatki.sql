-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 12, 2018 at 12:22 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kisikura_nejko`
--

-- --------------------------------------------------------

--
-- Table structure for table `Podatki`
--

CREATE TABLE `Podatki` (
  `ID` int(11) NOT NULL,
  `Ime` varchar(30) DEFAULT NULL,
  `Priimek` varchar(30) DEFAULT NULL,
  `Naslov` varchar(100) DEFAULT NULL,
  `Hisna_stevilka` varchar(10) DEFAULT NULL,
  `Obcina` varchar(100) DEFAULT NULL,
  `Email_naslov` varchar(100) DEFAULT NULL,
  `Telefonska_stevilka` varchar(30) DEFAULT NULL,
  `Nacin_placila` varchar(40) DEFAULT NULL,
  `Datum` varchar(40) DEFAULT NULL,
  `Ime_prejemnika` varchar(30) DEFAULT NULL,
  `Priimek_prejemnika` varchar(30) DEFAULT NULL,
  `Pismo` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Podatki`
--

INSERT INTO `Podatki` (`ID`, `Ime`, `Priimek`, `Naslov`, `Hisna_stevilka`, `Obcina`, `Email_naslov`, `Telefonska_stevilka`, `Nacin_placila`, `Datum`, `Ime_prejemnika`, `Priimek_prejemnika`, `Pismo`) VALUES
(1, 'matijatestira', 'bozickovic', 'prešernov trg', '5', 'ljubljana', 'kukmak', '01758987', 'Plačilo s kreditno kartico', 'do 12. 16. 2017', 'Bozickov', 'Peter', 'Tvoja kmetija smrdi. Slab si v nogometu. Ne razumeš fizike. Dobiš pico za darilo. Vesel božič in srečno novo leto!'),
(3, 'lovr', 'lvoro', 'lvor', 'lo', 'Kamnik', 'podobnik.lovro@gmail.com', 'lovr', 'Plačilo po povzetju', 'do 12. 12. 2017', 'Lovro', 'Podobnik', 'Tvoja hiša je ful lepa. Slab si v košarki. Ne znaš računat. Dobiš palačinke za darilo. Vesel božič in srečno novo leto!'),
(15, 'Joan Mayer', 'Benton', 'Melendez', 'Webb', 'Chapman', 'kukmak@gmail.com', NULL, 'Plačilo s kreditno kartico', 'do 12. 12. 2017', 'matija', 'zajec', 'Dragi Matija!\n\nTvoja kmetija smrdi. Slab si v nogometu. Dober si v fiziki. Vesel božič in srečno novo leto! ok dobro'),
(16, 'Lovro', 'Podobnik', 'Zale', '10a', 'Kamnik', 'podobnik.lovro@gmail.com', NULL, 'Plačilo po povzetju', 'do 20. 12. 2017', 'Lovro', 'Podobnik', 'Dragi Lovro!\n\nRad si v hiši. Slab si v košarki. Dober si v matematiki. Vesel božič in srečno novo leto!'),
(17, 'Lovro', 'Podobnik', 'Žale', '10a', 'Kamnik', 'podobnik.lovro@gmail.com', NULL, 'Plačilo po povzetju', 'do 12. 12. 2017', 'Lovro', 'Podobnik', 'Dragi Lovro!\n\nRad si v hiši. Slab si v košarki. Dober si v matematiki. Vesel božič in srečno novo leto!'),
(19, 'fff', 'ds', 's', 'sd', 'dsf', 'aaaaf@gmail.com', NULL, 'Predračun', 'do 20. 12. 2017', 'aaa', 'a', 'Vesel božič in srečno novo leto!'),
(20, 'fff', 'ds', 's', 'sd', 'dsf', 'aaaaf@gmail.com', NULL, 'Predračun', 'do 20. 12. 2017', 'aaa', 'a', 'Vesel božič in srečno novo leto!'),
(21, 'Lovro', 'Podobnik', 'žale', '10a', 'Kamnik', 'podobnik.lovro@gmail.com', NULL, 'Plačilo po povzetju', 'do 12. 12. 2017', 'Lovro', 'Podobnik', 'Dragi Lovro!\n\nRad si v hiši. Slab si v košarki. Dober si v matematiki. Vesel božič in srečno novo leto!'),
(24, 'afd', 'afd', 'afsd', 'afsd', 'afsd', 'sssss@gmail.com', NULL, 'Plačilo po povzetju', 'do 16. 12. 2017', 'adfs', '', 'Dragi Adfs,\n\nneizmerno sem bil vesel tvojega pisemca, ki sem ga našel v zvrhanem košu božičnih želja. Dečki, stari asdf let, jih imajo res veliko! Slišal sem, da si bil celo leto priden in prijazen, še posebej pa sem ponosen nate, ker si se v šoli zelo potrudil in si se v šoli zelo potrudil.\n\nSvojim škratom pomagalčkom sem še posebej naročil, da skrbno izdelajo darilo, ki si si ga letos zaželel. Verjamem, da si si to darilo res zaslužil.\n\nV delavnicah imajo sedaj naši škratki prav veliko dela, zunaj pritiska mraz in dežela na daljnem severu, od koder prihajamo, je odeta z debelo snežno odejo. Za to dolgo pot v Asdf si bodo Rudolf in njegovi prijatelji jelenčki dobro nabrusili kopita, da si pa ne bi umazal svoje snežnobele brade in božičnega oblačila, bom prosil tvoje starše, da pravočasno očistijo vaš dimnik.  Še naprej jih lepo ubogaj in zbiraj zvezdice dobrih del.\n\nPrepričan sem, da bo božična jelka do takrat že postavljena in okrašena, miza z dobrotami pa bogato obložena. Lahko mi prihraniš kakšen slasten piškotek, ker sem tudi jaz sladkosned.\n\nPozdravlja te in srčno objema\ntvoj Božiček'),
(25, NULL, NULL, NULL, NULL, NULL, 'aaaaf@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL),
(26, NULL, NULL, NULL, NULL, NULL, 'sara.bezjak007@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL),
(27, NULL, NULL, NULL, NULL, NULL, 'sara.bezjak007@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL),
(28, NULL, NULL, NULL, NULL, NULL, 'fasd@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL),
(29, NULL, NULL, NULL, NULL, NULL, 'fasd@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL),
(31, 'adsf', 'adfs', 'fasd', 'fasd', 'asdf', 'asdf@gmail.com', NULL, 'Plačilo po povzetju', 'do 16. 12. 2017', 'dsaf', '', 'Dragi Dsaf,\n\nneizmerno sem bil vesel tvojega pisemca, ki sem ga našel v zvrhanem košu božičnih želja. Dečki, stari fasd let, jih imajo res veliko! Slišal sem, da si bil celo leto priden in prijazen, še posebej pa sem ponosen nate, ker si se v šoli zelo potrudil in si se v šoli zelo potrudil.\n\nSvojim škratom pomagalčkom sem še posebej naročil, da skrbno izdelajo darilo, ki si si ga letos zaželel. Verjamem, da si si to darilo res zaslužil.\n\nV delavnicah imajo sedaj naši škratki prav veliko dela, zunaj pritiska mraz in dežela na daljnem severu, od koder prihajamo, je odeta z debelo snežno odejo. Za to dolgo pot v Fasd si bodo Rudolf in njegovi prijatelji jelenčki dobro nabrusili kopita, da si pa ne bi umazal svoje snežnobele brade in božičnega oblačila, bom prosil tvoje starše, da pravočasno očistijo vaš dimnik.  Še naprej jih lepo ubogaj in zbiraj zvezdice dobrih del.\n\nPrepričan sem, da bo božična jelka do takrat že postavljena in okrašena, miza z dobrotami pa bogato obložena. Lahko mi prihraniš kakšen slasten piškotek, ker sem tudi jaz sladkosned.\n\nPozdravlja te in srčno objema\ntvoj Božiček'),
(32, NULL, NULL, NULL, NULL, NULL, 'asdf@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL),
(33, NULL, NULL, NULL, NULL, NULL, 'mojmail@nomails.cnf', NULL, NULL, NULL, NULL, NULL, NULL),
(34, NULL, NULL, NULL, NULL, NULL, 'jurij@tovarnaidej.si', NULL, NULL, NULL, NULL, NULL, NULL),
(35, NULL, NULL, NULL, NULL, NULL, 'marko.skok89@gmai.com', NULL, NULL, NULL, NULL, NULL, NULL),
(36, NULL, NULL, NULL, NULL, NULL, 'borovinsek@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Podatki`
--
ALTER TABLE `Podatki`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Podatki`
--
ALTER TABLE `Podatki`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
