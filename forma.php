<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Tue Nov 28 2017 20:15:36 GMT+0000 (UTC)  -->
<html data-wf-page="5a1701aa6b2f3a0001d3a8d4" data-wf-site="5a16f3dfcc80bd0001ed8032">
<head>
  <meta charset="utf-8">
  <title>Forma</title>
  <meta content="Forma" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/bozickova.webflow.css" rel="stylesheet" type="text/css">

  <link href="css/custom.css" rel="stylesheet" type="text/css">

  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Oswald:200,300,400,500,600,700","Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Inconsolata:400,700"]  }});</script>

  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->


  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body class="body">
  <div class="container w-container"><img src="images/logo.svg" class="image">
    <div class="section">
      <div class="form-block w-form">

        <form action="sestavi-pismo.php" id="email-form" name="email-form" data-name="Email Form" method="post">

        	<label for="name-2" class="field-label">Vpiši tvoj email naslov:</label>
        <input type="email" class="w-input" maxlength="256" autofocus="true" name="email" data-name="email" placeholder="Email" id="email" required="">
          <h4 class="heading-2">Komu boš poslal pismo?</h4>

			<label required="" for="gender" class="field-label">Spol:</label>

            <label  class="control control--radio">Moški
		      <input id="moški" required="" type="radio" name="radio"/>
		      <div class="control__indicator"></div>
		    </label>
		     <label class="control control--radio">Ženski
		      <input id="ženska" type="radio" name="radio"/>
		      <div class="control__indicator"></div>
		    </label>

          <label for="name" class="field-label">Ime</label>
          <input type="text" class="w-input" maxlength="256" name="ime_p" data-name="name" placeholder="vpiši ime" id="name" required="">

          <label for="name" class="field-label">Starost</label>
          <input type="text" class="w-input" maxlength="256" name="starost" data-name="lastname" placeholder="vpiši starost" id="starost" required="">

          <label for="name" class="field-label">Kraj</label>
          <input type="text" class="w-input" maxlength="256" name="kraj" data-name="lastname" placeholder="vpiši kraj" id="kraj" required="">
<?php
error_reporting(0);
header('Cache-Control: no cache');
session_cache_limiter('private_no_expire');
$myfile = fopen("txt/var.txt", "r") or die("Unable to open file!");
$count = count( file("txt/var.txt"));

$a = array();
$b = array();
$c = array();
$f = array();
$d = array();
$l = 0;
for ($i = 0; $i < $count; $i++) {
  $line = explode("|", trim(fgets($myfile)));
  array_push($a, $line[0]);
  array_push($b, $line[1]);
  array_push($c, $line[2]);
  array_push($f, $line[3]);
  array_push($d, $line[4]);
  if ($i == 0) {
    echo '<label for=name class=field-label>' . $d[$i] . '</label>';
    echo '<select name="'.$d[$i].' id=mounth">';
  }
  if ($i > 0) {
    if ($d[$i] != $d[$i-1]) {
      echo '</select>';
      echo '<label for=name class=field-label>' . $d[$i] . '</label>';
      echo '<select name="'.$d[$i].' id=mounth">';
    }
  }
  echo '<option value="'.$b[$i].'">'.$b[$i].'</option>';
}
echo "</select>";
echo '<input type="hidden" id="a" name="a" value="' . implode("|", $a) . '">';
echo '<input type="hidden" id="b" name="b" value="' . implode("|", $b) . '">';
echo '<input type="hidden" id="c" name="c" value="' . implode("|", $c) . '">';
echo '<input type="hidden" id="f" name="f" value="' . implode("|", $f) . '">';
echo '<input type="hidden" id="d" name="d" value="' . implode("|", $d) . '">';
fclose($myfile);
?>
          <input type="submit" value="Nadaljuj" data-wait="Please wait..." onclick="insert()" class="submit-button w-button">
          <input type="hidden" id="content" name="content">
      </form>

      <div class="w-form-done">
        <div>Thank you! Your submission has been received!</div>
      </div>
      <div class="w-form-fail">
        <div>Oops! Something went wrong while submitting the form.</div>
      </div>

<div id="letter" style="display: none"; name="letter">
<?php
$letter = fopen("txt/mletter.txt", "r") or die("Unable to open file!");
$count = count( file("txt/mletter.txt"));
$pismo = array();
for ($i = 0; $i < $count; $i++) { 
  array_push($pismo, trim(fgets($letter)));
}
$text = implode("<br>", $pismo);
echo $text;
fclose($letter);
?>
</div>

<div id="letter2" style="display: none"; name="letter2">
<?php
$letter = fopen("txt/fletter.txt", "r") or die("Unable to open file!");
$count = count( file("txt/fletter.txt"));
$pismo = array();
for ($i = 0; $i < $count; $i++) { 
  array_push($pismo, trim(fgets($letter)));
}
$text = implode("<br>", $pismo);
echo $text;
fclose($letter);
?>
</div>

      </div>
    </div>
  </div>
  <script src="js/letters.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
  <script>	
(function($){
		var win,
      canvas, 
			ctx,
      circle = Math.PI * 2,
			snowflakes = [], 
			max_snowflakes = 50, 
			min_snowflake_radius = 1, 
			max_snowflake_radius = 5, 
      snowflake_max_velocity = 3,  
			gravity = new Vector(0,0.01),
			wind_speed = 0.1,
      wind_max_speed = 0.1,
			wind_entropy = 0.1,
			wind = new Vector((Math.random() - 0.5) * wind_speed, 0);
		function Vector(x,y)
		{
			this.x = x;
			this.y = y;
		}
		function Snowflake(size)
		{
			this.position = new Vector(0,0);
		 	this.velocity = new Vector(0,0);
		 	this.size = size;
      this.color = "rgba(255,255,255," + (max_snowflake_radius - this.size) +")";
		}
		function render()
		{
			var i, l = snowflakes.length, flake, min_x = canvas.width * -0.25, max_x = canvas.width * 1.25;
			canvas.width = canvas.width;
			for(i=0; i<l; i++)
			{
				flake = snowflakes[i];
        var half_size = flake.size * 0.5;
				flake.velocity.x = Math.min(snowflake_max_velocity, flake.velocity.x + gravity.x + wind.x + ((Math.random() - 0.5) * wind_entropy));
				flake.velocity.y = Math.min(snowflake_max_velocity, flake.velocity.y + gravity.y + wind.y + ((Math.random() - 0.5) * wind_entropy));
				flake.position.x += half_size * flake.velocity.x;
				flake.position.y += half_size * flake.velocity.y;
				if((flake.position.y < window.innerHeight) && (flake.position.x > min_x) && (flake.position.x < max_x))
				{
          if((flake.position.x > 0) && (flake.position.x < canvas.width) && (flake.position.y > 0))
          {
            ctx.fillStyle = flake.color;
            ctx.beginPath();
            ctx.arc(flake.position.x, flake.position.y, flake.size, 0, circle);
            ctx.closePath();
            ctx.fill();
          }
				}
				else
				{
					flake.position.x = (Math.random() * canvas.width * 3) - canvas.width;
          if(flake.position.x < 0 || flake.position.x > canvas.width)
          {
            flake.position.y = Math.random() * canvas.height;
          }
          else
          {
					  flake.position.y = Math.random() * -100;
          }
					flake.velocity = new Vector(0,0);
				}
			}
      wind.x += (Math.random() - 0.5) * wind_entropy * 0.125;
      wind.x = Math.min(wind_max_speed, wind.x);
			requestAnimationFrame(render);
		}
		function add_new_snowflake()
		{
			var w = canvas.width;
			var h = canvas.height;
			var size = Math.random() * (max_snowflake_radius - min_snowflake_radius) + min_snowflake_radius;
			var snowflake = new Snowflake(size);
			snowflake.position = new Vector((Math.random() * w * 3) - w, Math.random() * h);
			return snowflake;
		}
		function on_document_ready()
		{
      win = $(window);
			canvas = document.getElementById("snowfield");
			ctx = canvas.getContext("2d");      
      win.on('resize', resize).trigger('resize');
			for(var n=0; n<max_snowflakes; n++)
			{
				var snowflake = add_new_snowflake();
				snowflakes.push(snowflake);
			}
			requestAnimationFrame(render);
		}
    function resize()
    {
      canvas.height = window.innerHeight;
      canvas.width = window.innerWidth;
    }
		$(on_document_ready);
})(jQuery)

/*
Reference: http://jsfiddle.net/BB3JK/47/
*/

$('select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;
  
    $this.addClass('select-hidden'); 
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());
  
    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);
  
    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }
  
    var $listItems = $list.children('li');
  
    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });
  
    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });
  
    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});

</script>
</body>
</html>