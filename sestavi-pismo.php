<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Tue Nov 28 2017 20:15:36 GMT+0000 (UTC)  -->
<html data-wf-page="5a1dc0d043154a000153316d" data-wf-site="5a16f3dfcc80bd0001ed8032">
<head>
  <meta charset="utf-8">
  <title>sestavi-pismo</title>
  <meta content="sestavi-pismo" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/bozickova.webflow.css" rel="stylesheet" type="text/css">
  <link href="css/custom.css" rel="stylesheet" type="text/css">

  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Oswald:200,300,400,500,600,700","Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Inconsolata:400,700"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/favicon.ico" rel="shortcut icon" type="image/x-icon">
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body class="body">
  <div class="container w-container"><img src="images/logo.svg" class="image">
  		<br>
        <h4 align="center" class="heading-2-white">Božiček je tvoje pismo sestavil, skrbno ga preberi in uredi tako kot želiš sam.<br />
        Ne pozabi na osebno noto in male skrivnosti.</h4>

    <div class="section">

      <div class="form-block w-form">

        <form action="shopping-cart.php" id="email-form" name="email-form" data-name="Email Form" method="post">

	       <div class="paper">
				<div class="paper-content editable">
				<div id="edit" autofocus>
<?php
error_reporting(0);
header('Cache-Control: no cache');
session_cache_limiter('private_no_expire');
session_start();
$ime_p = $_POST['ime_p'];
$priimek_p = $_POST['priimek_p'];
$email = $_POST['email'];
$content = $_POST['content'];
$_SESSION["content"] = $content;
$_SESSION["ime_p"] = $ime_p;
$_SESSION["priimek_p"] = $priimek_p;
$_SESSION["email"] = $email;

$con = mysqli_connect("localhost", "Matt", "password", "snippets");

$sql = "INSERT INTO Podatki (Email_naslov) VALUES ('$email')";

mysqli_query($con, $sql);

mysqli_close($con);

echo $content;
?>
				</div>
				</div>
			</div>    
			<input type="submit" value="Nadaljuj" onclick="change()" data-wait="Please wait..." class="submit-button w-button">    	
			<input type="hidden" id="content" name="content">
			<input type="hidden" name="email" value="<?php echo $email; ?>">
			<input type="hidden" name="ime_p" value="<?php echo $ime_p; ?>">
			<input type="hidden" name="priimek_p" value="<?php echo $priimek_p; ?>">
        </form>

      </div>
    </div>
  </div>
  <script src="js/letters.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
  <script>	
(function($){
		var win,
      canvas, 
			ctx,
      circle = Math.PI * 2,
			snowflakes = [], 
			max_snowflakes = 50, 
			min_snowflake_radius = 1, 
			max_snowflake_radius = 5, 
      snowflake_max_velocity = 3,  
			gravity = new Vector(0,0.01),
			wind_speed = 0.1,
      wind_max_speed = 0.1,
			wind_entropy = 0.1,
			wind = new Vector((Math.random() - 0.5) * wind_speed, 0);
		function Vector(x,y)
		{
			this.x = x;
			this.y = y;
		}
		function Snowflake(size)
		{
			this.position = new Vector(0,0);
		 	this.velocity = new Vector(0,0);
		 	this.size = size;
      this.color = "rgba(255,255,255," + (max_snowflake_radius - this.size) +")";
		}
		function render()
		{
			var i, l = snowflakes.length, flake, min_x = canvas.width * -0.25, max_x = canvas.width * 1.25;
			canvas.width = canvas.width;
			for(i=0; i<l; i++)
			{
				flake = snowflakes[i];
        var half_size = flake.size * 0.5;
				flake.velocity.x = Math.min(snowflake_max_velocity, flake.velocity.x + gravity.x + wind.x + ((Math.random() - 0.5) * wind_entropy));
				flake.velocity.y = Math.min(snowflake_max_velocity, flake.velocity.y + gravity.y + wind.y + ((Math.random() - 0.5) * wind_entropy));
				flake.position.x += half_size * flake.velocity.x;
				flake.position.y += half_size * flake.velocity.y;
				if((flake.position.y < window.innerHeight) && (flake.position.x > min_x) && (flake.position.x < max_x))
				{
          if((flake.position.x > 0) && (flake.position.x < canvas.width) && (flake.position.y > 0))
          {
            ctx.fillStyle = flake.color;
            ctx.beginPath();
            ctx.arc(flake.position.x, flake.position.y, flake.size, 0, circle);
            ctx.closePath();
            ctx.fill();
          }
				}
				else
				{
					flake.position.x = (Math.random() * canvas.width * 3) - canvas.width;
          if(flake.position.x < 0 || flake.position.x > canvas.width)
          {
            flake.position.y = Math.random() * canvas.height;
          }
          else
          {
					  flake.position.y = Math.random() * -100;
          }
					flake.velocity = new Vector(0,0);
				}
			}
      wind.x += (Math.random() - 0.5) * wind_entropy * 0.125;
      wind.x = Math.min(wind_max_speed, wind.x);
			requestAnimationFrame(render);
		}
		function add_new_snowflake()
		{
			var w = canvas.width;
			var h = canvas.height;
			var size = Math.random() * (max_snowflake_radius - min_snowflake_radius) + min_snowflake_radius;
			var snowflake = new Snowflake(size);
			snowflake.position = new Vector((Math.random() * w * 3) - w, Math.random() * h);
			return snowflake;
		}
		function on_document_ready()
		{
      win = $(window);
			canvas = document.getElementById("snowfield");
			ctx = canvas.getContext("2d");      
      win.on('resize', resize).trigger('resize');
			for(var n=0; n<max_snowflakes; n++)
			{
				var snowflake = add_new_snowflake();
				snowflakes.push(snowflake);
			}
			requestAnimationFrame(render);
		}
    function resize()
    {
      canvas.height = window.innerHeight;
      canvas.width = window.innerWidth;
    }
		$(on_document_ready);
})(jQuery)

$('.editable').each(function(){
    this.contentEditable = true;
});

</script>
</body>
</html>